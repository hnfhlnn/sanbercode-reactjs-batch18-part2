import React from 'react';
import './Tugas9.css';

export default class Tugas9 extends React.Component {
    render() {
        return (
            <div className="App">
                <div className="content">
                    <header className="App-header">
                        <h1>Form Pembelian Buah</h1>
                    </header>
                    <body>
                        <form action="" method="post" >
                            <div class="group">
                                <label><b>Nama Pelanggan</b></label>
                                <input
                                    class="field"
                                    type="text"
                                />
                            </div>
                            <div class="group">
                                <label> <b>Daftar Item</b> </label>
                                <div class="field2">
                                    <input type="checkbox" id="semangka" name="semangka" value="semangka" />
                                    <label for="semangka"> Semangka </label>
                                    <br />
                                    <input type="checkbox" id="jeruk" name="jeruk" value="jeruk" />
                                    <label for="jeruk"> Jeruk </label>
                                    <br />
                                    <input type="checkbox" id="nanas" name="nanas" value="nanas" />
                                    <label for="nanas"> Nanas </label>
                                    <br />
                                    <input type="checkbox" id="salak" name="salak" value="salak" />
                                    <label for="salak"> Salak </label>
                                    <br />
                                    <input type="checkbox" id="anggur" name="anggur" value="anggur" />
                                    <label for="anggur"> Anggur </label>
                                </div>
                                <br />
                                <input class="btn" type="submit" name="btn" value="Kirim" />
                            </div>

                        </form>
                    </body>
                </div>
            </div>
        );
    }
}