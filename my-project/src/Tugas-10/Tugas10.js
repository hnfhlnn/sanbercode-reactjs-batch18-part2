import React from 'react';
import './Tugas10.css';

class Nama extends React.Component {
    render() {
      return <td>{this.props.nama}</td>;
    }
}

class Harga extends React.Component {
    render() {
      return <td>{this.props.harga}</td>;
    }
}

class Berat extends React.Component {
    render() {
        return <td>{this.props.beratkg} kg</td>;
    }
}

let dataHargaBuah = [
    { nama: "Semangka", harga: 10000, berat: 1000 },
    { nama: "Anggur", harga: 40000, berat: 500 },
    { nama: "Strawberry", harga: 30000, berat: 400 },
    { nama: "Jeruk", harga: 30000, berat: 1000 },
    { nama: "Mangga", harga: 30000, berat: 500 }
]

export default class Tugas10 extends React.Component {

    render() {
        return (
            <div className="App">
                <div className="content">
                    <header className="App-header">
                        <h1>Tabel Harga Buah</h1>
                    </header>
                    <body>
                        <table class="tabel">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Berat</th>
                                </tr>
                            </thead>
                            <tbody>
                                {dataHargaBuah.map(el => {
                                    return (
                                        <tr>
                                            <Nama nama={el.nama}/>
                                            <Harga harga={el.harga}/> 
                                            <Berat beratkg={el.berat/1000}/>  
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </body>
                </div>
            </div>
        );
    }
}