import React, {useState, useEffect} from "react"
import axios from "axios"
import './tugas13.css';

const BuahApi = ()=>{ 
    const [daftarBuah, setDaftarBuah] =  useState(null)
    const [input, setInput] =  useState({id: null, nama:"", harga:"", berat:null})
  
    useEffect( () => {
      if (daftarBuah === null){
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
          let daftarBuah = res.data
          setDaftarBuah(
            daftarBuah.map(el=> {
              return {id: el.id, nama: el.name, harga: el.price, berat: el.weight}
            })
          )      
        })
      }
    },[daftarBuah])
  
    const submitForm = (event) =>{
        event.preventDefault()
        
        if ( input.id === null){
          axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name: input.nama, price: input.harga, weight: input.berat})
          .then(res => {
            var data = res.data
            setDaftarBuah([...daftarBuah, {id: data.id, nama: data.name, harga: data.price, berat: data.weight}])
            setInput({id: null, nama:"", harga:"", berat:null})
          })
    
        }else{
    
          axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {name: input.nama, price: input.harga, weight: input.berat})
          .then(res => {
            var dataBuah = daftarBuah.map(x => {
              if (x.id === input.id){
                x.name = input.nama
                x.price = input.harga
                x.weight = input.berat
              }
              return x
            })
            setDaftarBuah(dataBuah)
            setInput({id: null, nama:"", harga:"", berat:null})
          })
        }
      }
    
      const handleDelete = (event) =>{
        var idBuah= parseInt(event.target.value) 
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
        .then(res => {
          var dataBuah = daftarBuah.filter(x=> x.id !== idBuah)
          setDaftarBuah(dataBuah)
        })
      }
/*
      const changeInputName = (event) =>{
        var value= event.target.value
        setInput({...input, name: value})
      } */

      const handleChangeNama = (event)=>{
        var value= event.target.value
        setInput({...input, nama: value})

      }

      const handleChangeHarga = (event)=>{
        var value= event.target.value
        setInput({...input, harga: value})
      }

      const handleChangeBerat = (event)=>{
        var value= event.target.value
        setInput({...input, berat: value})
      }
    
      const handleEdit = (event) =>{
        var idBuah= parseInt(event.target.value)
        var buah = daftarBuah.find(x=> x.id === idBuah)
    
        setInput({id: idBuah, nama: buah.nama, harga: buah.harga, berat: buah.berat})
      }
    
        return (
            <div className="App">
                <div className="content">
                    <header className="App-header">
                        <h1>Tabel Harga Buah</h1>
                    </header>
                    <body>
                        <table class="tabel">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Berat (Kg)</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {daftarBuah !== null && (
                                    daftarBuah.map((el,index) => {
                                        return (
                                            <tr key={el.id}>
                                                <td class="satu">{index+1}.</td>
                                                <td class="dua">{el.nama}</td>
                                                <td class="dua">{el.harga}</td> 
                                                <td class="dua">{el.berat/1000}</td>  
                                                <td class="tiga">
                                                    <button type="button" value={el.id} onClick={handleDelete}>
                                                        Delete
                                                    </button>
                                                    <button type="button" value={el.id} onClick={handleEdit}>
                                                        Edit
                                                    </button>
                                               </td>
                                            </tr>
                                        )
                                    })
                                )}
                            </tbody>
                        </table>
                        {/* Form */}
                        <header className="App-header">
                            <h1>Form Daftar Buah</h1>
                        </header>
                        <form onSubmit={submitForm}>
                            <div class="grup">
                                <label>
                                    Nama: 
                                </label> 
                                <input type="text" value={input.nama} onChange={handleChangeNama} required/>
                            </div>
                            <div class="grup">
                            <label>
                                Harga: 
                            </label> 
                            <input type="text" value={input.harga} onChange={handleChangeHarga} required/>
                            </div>
                            <div class="grup">
                                <label>
                                    Berat (gram): 
                                </label> 
                                <input type="text" value={input.berat} onChange={handleChangeBerat} required/>
                            </div>        
                            <button>submit</button>
                        </form>
                    </body>
                </div>
            </div>
        )
    }

export default BuahApi