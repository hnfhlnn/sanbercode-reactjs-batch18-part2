import React, { Component } from 'react';
import './timer.css';

function convert(date) {
    let hours = date.getHours();
    let ket;
    if (hours>12) {
        hours=hours-12;
        ket="PM"
    } else {
        ket="AM"
    }

    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;


    return `${hours}:${mins}:${secs} ${ket}`;

}

class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            time: 100,
            curTime: convert(new Date())
        }
    }

    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start })
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: this.state.time - 1,
            curTime: convert(new Date())
        });
    }


    render() {
        var input;
        var input2;
            if(this.state.time >= 0) {
                input = 
                    <h1 class="satu">
                        hitung mundur: { this.state.time }
                    </h1>;
                input2 =
                    <h1 class="dua">
                        sekarang jam: { this.state.curTime }
                    </h1>;
                    
            }
        return (
            <div class="App">
                <div class="isi">
                    {input2}
                    {input}
                </div>
            </div>

        )
    }
}

export default Timer