import React from 'react';
import './tugas12.css';

class Nama extends React.Component {
    render() {
      return <td class="dua">{this.props.nama}</td>;
    }
}

class Harga extends React.Component {
    render() {
      return <td class="dua">{this.props.harga}</td>;
    }
}

class Berat extends React.Component {
    render() {
        return <td class="dua">{this.props.beratkg} kg</td>;
    }
}


export default class Tugas12 extends React.Component {
    constructor(props){
        super(props)
        this.state ={
         Buah : [
           {nama:"semangka",harga:10000,berat:1000},
           {nama:"Anggur",harga:40000,berat:500},
           {nama:"strawbery",harga:30000,berat:400},
           {nama:"Jeruk",harga:30000,berat:1000},
           {nama:"Mangga",harga:30000,berat:500},
         ],
         inputName : "",
         inputHarga : "",
         inputBerat : "",   
         index: -1
        }
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRemoveItem = this.onRemoveItem.bind(this);
        this.editItem = this.editItem.bind(this);
      }
    
      handleChange(event){
        this.setState({[event.target.name]: event.target.value});
      }
    
      handleSubmit(event){
        event.preventDefault();
        if (this.state.index===-1) {
          this.setState({
            Buah: [...this.state.Buah, {nama:this.state.inputName,harga:this.state.inputHarga,berat:this.state.inputBerat}],
            inputName : "",
            inputHarga : "",
            inputBerat : "",
            index: -1
          })
        } else {
          this.state.Buah[this.state.index]={nama:this.state.inputName,harga:this.state.inputHarga,berat:this.state.inputBerat}
          this.setState({
            Buah: this.state.Buah,
            inputName : "",
            inputHarga : "",
            inputBerat : "",
            index: -1
          })
        }
      }
    
      onRemoveItem(event){
        let index = event.target.value;
        this.state.Buah.splice(index,1);
        this.setState({Buah: this.state.Buah})
      }
    
      editItem(event){
        let index = event.target.value;
        this.setState({
          inputName: this.state.Buah[index].nama,
          inputHarga: this.state.Buah[index].harga,
          inputBerat: this.state.Buah[index].berat,
          index})
      }

    render() {
        return (
            <div className="App">
                <div className="content">
                    <header className="App-header">
                        <h1>Tabel Harga Buah</h1>
                    </header>
                    <body>
                        <table class="tabel">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Berat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.Buah.map((el,index) => {
                                    return (
                                        <tr>
                                            <td class="satu">{index+1}.</td>
                                            <Nama nama={el.nama}/>
                                            <Harga harga={el.harga}/> 
                                            <Berat beratkg={el.berat/1000}/>  
                                            <td class="tiga">
                                                <button type="button" value={index} onClick={this.onRemoveItem}>
                                                    Delete
                                                </button>
                                                <button type="button" value={index} onClick={this.editItem}>
                                                    Edit
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        {/* Form */}
                        <header className="App-header">
                            <h1>Form Daftar Buah</h1>
                        </header>
                        <form onSubmit={this.handleSubmit}>
                            <div class="grup">
                                <label>
                                    Nama: 
                                </label> 
                                <input type="text" name="inputName" value={this.state.inputName} onChange={this.handleChange}/>
                            </div>
                            <div class="grup">
                            <label>
                                Harga: 
                            </label> 
                            <input type="number" name="inputHarga" value={this.state.inputHarga} onChange={this.handleChange}/>
                            </div>
                            <div class="grup">
                                <label>
                                    Berat (gram): 
                                </label> 
                                <input type="number" name="inputBerat" value={this.state.inputBerat} onChange={this.handleChange}/>
                            </div>        
                            <button>submit</button>
                        </form>
                    </body>
                </div>
            </div>
        );
    }
}